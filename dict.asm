
%include "lib.inc"


section .text
global find_word



find_word:
    .loop:
        test rsi, rsi
        jz .notfound
        push rsi
        push rdi
        add rsi, 8
        mov rax, rsi
        call string_equals
        test rax, rax
        jne .found
        pop rdi
        pop rsi
        mov rsi, [rsi]
        jmp .loop
    .found:
        pop rdi
        pop rax
        add rax, 8
        push rax
        mov rdi, rax
        call string_length
        add rax, [rsp]
        inc rax
        pop rdi
        ret
    .notfound:
        xor rax, rax
        ret