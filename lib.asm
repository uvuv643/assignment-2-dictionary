%define NULL_TERMINATOR 0
%define EXIT 60
%define STDOUT 1
%define STDIN 0
%define STDERR 2
%define NEW_LINE 0xA
%define SYSCALL_PRINT 1
%define SYSCALL_READ 0
%define SPACE 0x20
%define TAB 0x9
%define MINN 0
%define MAXX 0x39


global exit
global string_length
global print_string
global print_string_stderr
global print_newline
global string_equals
global string_copy
global read_word

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length: ; works
    .loop:
        xor rax, rax
    .count:
        cmp byte[rdi + rax], NULL_TERMINATOR
        je .end
        inc rax
        jmp .count
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: ; works
    mov rsi, rdi
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rdi, STDOUT
    mov rax, SYSCALL_PRINT
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char: ; works
    push rdi
    mov rsi, rsp ; получить адрес символа
    mov rax, SYSCALL_PRINT
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline: ; works
    push rdi
    mov rdi, NEW_LINE
    call print_char
    pop rdi
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: ; works
    mov rax, rdi
    mov r8, 10
    push 0x0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, NEW_LINE
        push rdx
        cmp rax, r8
        jae .loop

    add rax, NEW_LINE
    cmp rax, NEW_LINE
    je .print_next
    push rax

    .print_next:
        pop rax
        test rax, rax
        je .end
        mov rdi, rax
        call print_char
        jmp .print_next

    .end:
        ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int: ; works
    cmp rdi, 0
    jge print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    call string_length
    mov r8, rax
    mov rdi, rsi
    call string_length
    mov r9, rax
    pop rdi
    cmp r8, r9
    jne .notequals
    .equals_length:
        mov rcx, rax
        test rcx, rcx
        jz .equals
        .new_symbol:
            mov al, byte[rdi + rcx]
            mov ah, byte[rsi + rcx]
            cmp al, ah
            jne .notequals
            loop .new_symbol
    .equals:
        mov rax, 1
        ret
    .notequals:
        mov rax, 0
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYSCALL_READ
    mov rdi, STDIN
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi ; начало буффера
    mov r9, rsi ; длина буффера
    mov r10, rdi ; answer
    sub r9, 1 ; \0

    .skip_symbols:
        call read_char
        cmp rax, SPACE
        je .skip_symbols
        cmp rax, TAB
        je .skip_symbols
        cmp rax, NEW_LINE
        je .skip_symbols

    .read_symbols:
        cmp rax, 0
        je .create_word
        cmp rax, SPACE
        je .create_word
        cmp rax, TAB
        je .create_word
        cmp rax, NEW_LINE
        je .create_word

        mov [r8], rax
        add r8, 1
        sub r9, 1
        cmp r9, 1
        jb .error

        call read_char

        jmp .read_symbols

    .create_word:
        mov byte[r8], 0
        mov rdi, r10
        call string_length
        mov rdx, rax
        mov rax, r10
        ret

    .error:
        mov rax, 0
        mov rdx, 0
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    .loop:
        cmp byte[rdi + rdx], 0
        je .exit
        cmp byte[rdi + rdx], MINN
        jl .exit
        cmp byte[rdi + rdx], MAXX
        jg .exit
        mov cl, byte[rdi + rdx]
        sub cl, MINN
        imul rax, 10
        add rax, rcx
        inc rdx
        jmp .loop
    .exit:
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    je .neg
    call parse_uint
    jmp .exit
    .neg:
        add rdi, 1
        call parse_uint
        neg rax
        add rdx, 1
    .exit:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx
        je .error
        cmp byte[rdi + rax], 0x0
        je .exit
        mov cl, [rdi + rax]
        mov byte[rsi + rax], cl
        inc rax
        jmp .loop
    .error:
        mov rax, 0
        ret
    .exit:
        mov byte[rsi + rax], 0x0
        ret

print_string_stderr: ; works
    mov rsi, rdi
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rdi, STDERR
    mov rax, 1
    syscall
    ret