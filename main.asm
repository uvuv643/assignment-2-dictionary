%include "words.inc"
%include "lib.inc"
extern find_word

section .rodata
not_found: db "Not found", 0
input_error: db "Input error", 0

section .bss
buffer: times 4 dq 0

section .text

global _start

_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	test rdx, rdx
	jz .error
	mov rdi, rax
	mov rsi, next
	call find_word
	test rax, rax
	je .not_found
	.found:
        mov rdi, rax
        call print_string
        call print_newline
        jmp .exit
	.not_found:
        mov rdi, not_found
        call print_string_stderr
        call print_newline
        jmp .exit
	.error:
        mov rdi, input_error
        call print_string_stderr
        call print_newline
	.exit:
        call exit
	
